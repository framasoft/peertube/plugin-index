import { isTestInstance } from './server/helpers/core-utils.js'
if (isTestInstance()) {
  require('source-map-support').install()
}

import bodyParser from 'body-parser'
import express from 'express'
import cors from 'cors'
import morgan from 'morgan'
import { apiRouter } from './server/controllers/api/index.js'
import { logger } from './server/helpers/logger.js'
import { API_VERSION, CONFIG } from './server/initializers/constants.js'
// Initialize database and models
import { initDatabaseModels, sequelizeTypescript } from './server/initializers/database.js'
import { RequestsScheduler } from './server/lib/requests-scheduler.js'

const app = express()
app.set('trust proxy', [ 'loopback' ])

initDatabaseModels(false)
  .then(() => onDatabaseInitDone())
  .catch(err => logger.error({ err }, 'Cannot initialize database'))

app.use(morgan('combined', {
  stream: { write: logger.info.bind(logger) }
}))

app.use(bodyParser.json({
  type: [ 'application/json', 'application/*+json' ],
  limit: '500kb'
}))
app.use(bodyParser.urlencoded({ extended: false }))

// ----------- Views, routes and static files -----------

app.use(cors())

const apiRoute = '/api/' + API_VERSION
app.use(apiRoute, apiRouter)

// ----------- Errors -----------

// Catch 404
app.use((_req, res: express.Response) => {
  res.status(404).end()
})

app.use(function (err, req, res, next) {
  let error = 'Unknown error.'
  if (err) {
    error = err.stack || err.message || err
  }

  logger.error('Error in controller.', { error })
  return res.status(err.status || 500).end()
})

// ----------- Run -----------

async function onDatabaseInitDone () {
  const port = CONFIG.LISTEN.PORT

  await sequelizeTypescript.sync()

  // ----------- Make the server listening -----------
  app.listen(port, () => {
    logger.info('Server listening on port %d', port)

    RequestsScheduler.Instance.enable()
  })
}
