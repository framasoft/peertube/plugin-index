import express from 'express'
import RateLimit from 'express-rate-limit'
import type { PeertubePluginLatestVersionRequest } from '@peertube/peertube-types'
import { getFormattedObjects } from '../../helpers/utils.js'
import { asyncMiddleware } from '../../middlewares/async.js'
import { setDefaultPagination } from '../../middlewares/pagination.js'
import { setDefaultSort } from '../../middlewares/sort.js'
import { pluginGetLatestVersionValidator, pluginsListValidator } from '../../middlewares/validators/instances.js'
import { paginationValidator } from '../../middlewares/validators/pagination.js'
import { pluginsSortValidator } from '../../middlewares/validators/sort.js'
import { PluginModel } from '../../models/plugin.js'

const rateLimiter = RateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100
})

const pluginsRouter = express.Router()

pluginsRouter.get('/',
  pluginsListValidator,
  paginationValidator,
  pluginsSortValidator,
  setDefaultSort,
  setDefaultPagination,
  asyncMiddleware(listPlugins)
)

pluginsRouter.post('/latest-version',
  rateLimiter,
  pluginGetLatestVersionValidator,
  asyncMiddleware(getLatestPluginVersion)
)

// ---------------------------------------------------------------------------

export {
  pluginsRouter
}

// ---------------------------------------------------------------------------

async function listPlugins (req: express.Request, res: express.Response) {
  const pluginType = req.query.pluginType
  const currentPeerTubeEngine = req.query.currentPeerTubeEngine
  const search = req.query.search

  const resultList = await PluginModel.listForApi({
    start: req.query.start,
    count: req.query.count,
    sort: req.query.sort,
    search,
    pluginType,
    currentPeerTubeEngine
  })

  return res.json(getFormattedObjects(resultList.data, resultList.total))
}

async function getLatestPluginVersion (req: express.Request, res: express.Response) {
  const body: PeertubePluginLatestVersionRequest = req.body

  const npmNames = body.npmNames
  const currentPeerTubeEngine = body.currentPeerTubeEngine

  const result = await PluginModel.getLatestVersions(npmNames, currentPeerTubeEngine)

  return res.json(result)
}
