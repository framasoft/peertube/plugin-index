import express from 'express'
import { badRequest } from '../../helpers/utils.js'
import { pluginsRouter } from './plugins.js'

const apiRouter = express.Router()

apiRouter.use('/plugins', pluginsRouter)
apiRouter.use('/ping', pong)
apiRouter.use('/*', badRequest)

// ---------------------------------------------------------------------------

export { apiRouter }

// ---------------------------------------------------------------------------

function pong (req: express.Request, res: express.Response) {
  return res.send('pong').status(200).end()
}
