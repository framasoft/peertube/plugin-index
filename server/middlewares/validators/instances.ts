import express from 'express'
import { body, query } from 'express-validator'
import { logger } from '../../helpers/logger.js'
import { areValidationErrors } from './utils.js'
import { arePluginNpmNamesValid, isPluginTypeValid, isPluginVersionValid } from '../../helpers/custom-validators/plugins.js'
import { exists } from '../../helpers/custom-validators/misc.js'

const pluginsListValidator = [
  query('pluginType')
    .optional()
    .toInt()
    .custom(isPluginTypeValid).withMessage('Should have a valid plugin type'),
  query('currentPeerTubeEngine')
    .optional()
    .custom(isPluginVersionValid).withMessage('Should have a valid current peertube engine'),
  query('search')
    .optional()
    .custom(exists).withMessage('Should have a valid search'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.query }, 'Checking plugins list parameters')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

const pluginGetLatestVersionValidator = [
  body('npmNames')
    .custom(arePluginNpmNamesValid)
    .withMessage('Should have valid npm name'),
  body('currentPeerTubeEngine')
    .optional()
    .custom(isPluginVersionValid).withMessage('Should have a valid current peertube engine'),

  (req: express.Request, res: express.Response, next: express.NextFunction) => {
    logger.debug({ parameters: req.body }, 'Checking pluginGetLatestVersionValidator parameters')

    if (areValidationErrors(req, res)) return

    return next()
  }
]

// ---------------------------------------------------------------------------

export {
  pluginsListValidator,
  pluginGetLatestVersionValidator
}
