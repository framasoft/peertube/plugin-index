import { SORTABLE_COLUMNS } from '../../initializers/constants.js'
import { checkSort, createSortableColumns } from './utils.js'

const SORTABLE_PLUGINS_COLUMNS = createSortableColumns(SORTABLE_COLUMNS.PLUGINS)

const pluginsSortValidator = checkSort(SORTABLE_PLUGINS_COLUMNS)

// ---------------------------------------------------------------------------

export {
  pluginsSortValidator
}
