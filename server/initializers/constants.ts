import config from 'config'
import { isTestInstance } from '../helpers/core-utils.js'

const API_VERSION = 'v1'

const CONFIG = {
  LISTEN: {
    PORT: config.get<number>('listen.port')
  },
  DATABASE: {
    DBNAME: config.get<string>('database.name'),
    HOSTNAME: config.get<string>('database.hostname'),
    PORT: config.get<number>('database.port'),
    USERNAME: config.get<string>('database.username'),
    PASSWORD: config.get<string>('database.password')
  },
  LOG: {
    LEVEL: config.get<string>('log.level')
  },
  OFFICIAL_PLUGINS: {
    NPM_NAMES: config.get<string[]>('official-plugins.npm-name')
  },
  RECOMMENDED_PLUGINS: {
    NPM_NAMES: config.get<string[]>('recommended-plugins.npm-name')
  },
  INSTANCES_INDEX: {
    STATS_URL: config.get<string>('instances-index.stats-url')
  }
}

const CONSTRAINTS_FIELDS = {
  ACTORS: {
    URL: { min: 3, max: 2000 } // Length
  },
  PLUGINS: {
    NAME: { min: 1, max: 214 }, // Length
    DESCRIPTION: { min: 1, max: 20000 } // Length
  }
}

const SORTABLE_COLUMNS = {
  PLUGINS: [ 'id', 'npmName', 'createdAt', 'popularity', 'trending' ]
}

const PAGINATION_COUNT_DEFAULT = 20

let SCHEDULER_INTERVAL = 60000 * 60 * 1 // 1 hour

if (isTestInstance()) {
  SCHEDULER_INTERVAL = 10000
}

export {
  CONFIG,
  API_VERSION,
  PAGINATION_COUNT_DEFAULT,
  SORTABLE_COLUMNS,
  SCHEDULER_INTERVAL,
  CONSTRAINTS_FIELDS
}
