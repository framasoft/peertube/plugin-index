import { literal, Op, Order, WhereOptions } from 'sequelize'
import { AllowNull, Column, CreatedAt, DataType, Default, HasMany, Model, Table, UpdatedAt } from 'sequelize-typescript'
import { PeerTubePluginIndex, PeertubePluginLatestVersionResponse, PluginType_Type } from '@peertube/peertube-types'
import { AttributesOnly } from '@peertube/peertube-types/peertube-typescript-utils'
import { CONFIG } from '../initializers/constants.js'
import { PluginVersionModel } from './plugin-version.js'
import { getSort } from './utils.js'
import { Literal } from 'sequelize/types/utils'

@Table({
  tableName: 'plugin',
  indexes: [
    {
      fields: [ 'npmName' ],
      unique: true
    }
  ]
})
export class PluginModel extends Model<Partial<AttributesOnly<PluginModel>>> {

  @AllowNull(false)
  @Column
  npmName: string

  @AllowNull(false)
  @Column
  type: PluginType_Type

  @AllowNull(false)
  @Column(DataType.REAL)
  popularity: number

  @AllowNull(false)
  @Default(0)
  @Column
  publicInstallations: number

  @AllowNull(false)
  @Column
  description: string

  @AllowNull(false)
  @Column
  homepage: string

  @AllowNull(false)
  @Default(false)
  @Column
  blacklisted: boolean

  @AllowNull(false)
  @Default(false)
  @Column
  official: boolean

  @AllowNull(false)
  @Default(false)
  @Column
  recommended: boolean

  @CreatedAt
  createdAt: Date

  @UpdatedAt
  updatedAt: Date

  @HasMany(() => PluginVersionModel, {
    foreignKey: {
      allowNull: false
    },
    onDelete: 'cascade'
  })
  Plugins: Awaited<PluginModel[]>

  static listForApi (options: {
    start: number
    count: number
    sort: string
    search?: string
    pluginType?: PluginType_Type
    currentPeerTubeEngine?: string
  }) {
    const and: WhereOptions[] = [
      {
        blacklisted: false
      }
    ]

    const query = {
      attributes: {
        include: [
          [ PluginModel.buildLatestVersion(options.currentPeerTubeEngine), 'latestVersion' ] as [ Literal, string ],
          [ PluginModel.buildTrending(), 'trending' ] as [ Literal, string ]
        ]
      },
      offset: options.start,
      limit: options.count,
      order: getSort(options.sort, [ 'id', 'ASC' ]) as Order
    }

    if (options.pluginType) {
      and.push({
        type: options.pluginType
      })
    }

    if (options.currentPeerTubeEngine) {
      const sanitized = this.sanitizeVersion(options.currentPeerTubeEngine)

      and.push(
        literal(
          `EXISTS (SELECT 1 FROM "pluginVersion" ` +
          `WHERE "minPeerTubeEngine" <= string_to_array('${sanitized}', '.')::int[] ` +
          `AND "pluginVersion"."pluginId" = "PluginModel"."id")`
        )
      )
    }

    if (options.search) {
      const search = '%' + options.search + '%'

      and.push({
        [Op.or]: [
          {
            npmName: {
              [Op.iLike]: search
            }
          },
          {
            description: {
              [Op.iLike]: search
            }
          }
        ]
      })
    }

    Object.assign(query, { where: and })

    return PluginModel.findAndCountAll(query)
      .then(({ rows, count }) => {
        return {
          data: rows,
          total: count
        }
      })
  }

  static loadPluginIdByVersion (npmName: string, version: string) {
    const query = {
      attributes: [ 'id' ],
      where: {
        npmName
      },
      include: [
        {
          model: PluginVersionModel,
          required: true,
          where: {
            rawVersion: version
          }
        }
      ]
    }

    return PluginModel.findOne(query)
  }

  static getLatestVersions (npmNames: string[], currentPeerTubeEngine: string) {
    const query = {
      raw: true,
      attributes: [
        'npmName',
        [ PluginModel.buildLatestVersion(currentPeerTubeEngine), 'latestVersion' ] as [ Literal, string ]
      ],
      where: {
        npmName: {
          [Op.in]: npmNames
        }
      }
    }

    return PluginModel.findAll(query) as unknown as Promise<PeertubePluginLatestVersionResponse>
  }

  static getPluginWithoutIds (ids: number[], type: PluginType_Type) {
    const query = {
      where: {
        id: {
          [Op.notIn]: ids
        },
        type
      }
    }

    return PluginModel.findAll(query)
  }

  toFormattedJSON (): PeerTubePluginIndex & { recommended: boolean } {
    return {
      npmName: this.npmName,
      description: this.description,
      homepage: this.homepage,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,

      official: this.official,
      recommended: this.recommended,

      popularity: this.popularity,
      latestVersion: this.get('latestVersion') as string
    }
  }

  private static sanitizeVersion (version: string) {
    return version.split('-')[0]
  }

  private static buildLatestVersion (maxPTVersion?: string) {
    let whereAnd = ''

    if (maxPTVersion) {
      // We don't need the -rc.1, -beta etc par
      const sanitized = this.sanitizeVersion(maxPTVersion)

      whereAnd = `AND "minPeerTubeEngine" <= string_to_array('${sanitized}', '.')::int[] `
    }

    return literal('(SELECT "rawVersion" FROM "pluginVersion" WHERE "pluginVersion"."pluginId" = "PluginModel"."id" ' +
      whereAnd +
      'ORDER BY "version" DESC LIMIT 1)')
  }

  private static buildTrending () {
    return literal('"publicInstallations" * ("official"::int * 3 + "recommended"::int * 2 + 1)')
  }
}
