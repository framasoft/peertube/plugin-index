import { doJSONRequest } from '../helpers/requests.js'

export interface NPMObject {
  package: {
    name: string
    version: string
    description: string

    links: {
      homepage: string
    }
  }

  score: {
    detail: {
      popularity: number
    }
  }
}

export async function searchOnNPM (keywordsArg: string, options: { from: number, size: number }) {
  const keywords = encodeURIComponent(keywordsArg).replace('%2B', '+')

  const uri = `https://registry.npmjs.org/-/v1/search?text=keywords:${keywords}&size=${options.size}&from=${options.from}`

  const { body } = await doJSONRequest(uri)

  return body as { objects: NPMObject[] }
}

export async function getPackage (npmName: string, version: string) {
  const uri = `https://registry.npmjs.org/${npmName}/${version}`

  const { body } = await doJSONRequest(uri)

  return body
}
