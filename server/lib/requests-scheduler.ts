import { PluginPackageJSON, PluginType_Type } from '@peertube/peertube-types'
import { isPluginVersionValid, sanitizeAndCheckPackageJSONValid } from '../helpers/custom-validators/plugins.js'
import { logger } from '../helpers/logger.js'
import { CONFIG, SCHEDULER_INTERVAL } from '../initializers/constants.js'
import { PluginModel } from '../models/plugin.js'
import { PluginVersionModel } from '../models/plugin-version.js'
import { getPackage, NPMObject, searchOnNPM } from './npm.js'
import { PluginType } from '../types/index.js'
import { RegisteredPluginStat, getInstancesPluginsStats } from './stats.js'

export class RequestsScheduler {

  private interval: any
  private isRunning = false

  private static instance: RequestsScheduler

  private constructor () { }

  enable () {
    logger.info('Enabling request scheduler.')

    this.interval = setInterval(() => {
      this.execute()
        .catch(err => logger.error({ err }, 'Error in request scheduler'))
    }, SCHEDULER_INTERVAL)

    this.execute()
      .catch(err => logger.error({ err }, 'Error in request scheduler'))
  }

  disable () {
    clearInterval(this.interval)
  }

  async execute () {
    // Already running?
    if (this.isRunning === true) return

    this.isRunning = true

    try {
      logger.info('Running NPM search scheduler.')

      logger.info('Getting instances plugins stats.')
      const stats = await getInstancesPluginsStats()

      logger.debug({ stats }, 'Got instances plugins stats.')

      logger.info('Searching PeerTube themes.')
      await this.scrape(PluginType.THEME, stats.registeredThemes)

      logger.info('Searching PeerTube plugins.')
      await this.scrape(PluginType.PLUGIN, stats.registeredPlugins)

      this.isRunning = false
    } catch (err) {
      this.isRunning = false
      throw err
    }
  }

  private async scrape (type: PluginType_Type, stats: RegisteredPluginStat[]) {
    const keywords = type === PluginType.PLUGIN
      ? 'peertube+plugin'
      : 'peertube+theme'

    let from = 0
    const options = {
      detailed: true,
      size: 50,
      from: undefined
    }

    let objects: any[] = []
    let pluginIdsProcessed: number[] = []

    do {
      options.from = from

      const res = await searchOnNPM(keywords, options)
      objects = res.objects

      const newIds = await this.addPlugins(objects, type, stats)
      pluginIdsProcessed = pluginIdsProcessed.concat(newIds)

      from += options.size
    } while (objects.length !== 0)

    const notExistingPlugins = await PluginModel.getPluginWithoutIds(pluginIdsProcessed, type)
    for (const notExistingPlugin of notExistingPlugins) {
      logger.info('Removing not existing plugin %s.', notExistingPlugin.npmName)
      await notExistingPlugin.destroy()
    }
  }

  private async addPlugins (objects: NPMObject[], pluginType: PluginType_Type, stats: RegisteredPluginStat[]) {
    const pluginIdsProcessed: number[] = []

    for (const o of objects) {
      const npmPackage = o.package
      // Filtering bad results.
      // Note: some plugins are returned when searching themes, and vice-versa.
      if (pluginType === PluginType.PLUGIN && !npmPackage.name.startsWith('peertube-plugin-')) {
        continue
      }
      if (pluginType === PluginType.THEME && !npmPackage.name.startsWith('peertube-theme-')) {
        continue
      }

      logger.info('Adding plugin/theme %s.', npmPackage.name)
      logger.debug({ object: o }, 'Plugin/theme %s.', npmPackage.name)

      try {
        const existingPlugin = await PluginModel.loadPluginIdByVersion(npmPackage.name, npmPackage.version)
        if (process.env.FORCE_PROCESS_EXISTING_PLUGIN_VERSION !== 'true' && existingPlugin) {
          pluginIdsProcessed.push(existingPlugin.id)
          continue
        }

        const packageJSON: PluginPackageJSON = await getPackage(npmPackage.name, npmPackage.version)
        if (!sanitizeAndCheckPackageJSONValid(packageJSON, pluginType)) {
          logger.error({ packageJSON }, 'Cannot add plugin %s@%s because packageJSON is not valid.', npmPackage.name, npmPackage.version)
          continue
        }

        const minPeerTubeEngine = this.getMinPeerTubeSplittedVersion(packageJSON.engine.peertube)
        if (!minPeerTubeEngine) {
          logger.error({ packageJSON }, 'Invalid PeerTube engine in package.json.')
          continue
        }

        const version = this.getSplittedVersion(npmPackage.version)

        const [ plugin ] = await PluginModel.upsert({
          npmName: npmPackage.name,
          type: pluginType,
          popularity: o.score.detail.popularity * 100,
          publicInstallations: stats.find(p => p.npmName === npmPackage.name)?.total || 0,
          official: CONFIG.OFFICIAL_PLUGINS.NPM_NAMES.includes(npmPackage.name),
          recommended: CONFIG.RECOMMENDED_PLUGINS.NPM_NAMES.includes(npmPackage.name),
          description: packageJSON.description,
          homepage: packageJSON.homepage
        }, { returning: true })

        pluginIdsProcessed.push(plugin.id)

        await PluginVersionModel.findOrCreate({
          where: {
            pluginId: plugin.id,
            rawVersion: npmPackage.version
          },
          defaults: {
            pluginId: plugin.id,
            rawVersion: npmPackage.version,
            version,
            rawPeerTubeEngine: packageJSON.engine.peertube,
            minPeerTubeEngine
          }
        })
      } catch (err) {
        logger.error({ err }, 'Cannot add plugin %s in database.', npmPackage.name)
      }
    }

    return pluginIdsProcessed
  }

  private getMinPeerTubeSplittedVersion (peertubeEngine: string) {
    const min = peertubeEngine.replace(/^>=/, '')

    if (!isPluginVersionValid(min)) return undefined

    return this.getSplittedVersion(min)
  }

  private getSplittedVersion (version: string) {
    return version.split('.')
              .map(p => parseInt(p, 10))
  }

  static get Instance () {
    return this.instance || (this.instance = new this())
  }
}
