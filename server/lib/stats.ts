import { CONFIG } from '../initializers/constants.js'
import { doJSONRequest } from '../helpers/requests.js'

export type RegisteredPluginStat = { npmName: string, total: number }

export async function getInstancesPluginsStats () {
  const { body } = await doJSONRequest(CONFIG.INSTANCES_INDEX.STATS_URL)

  return {
    registeredPlugins: body.registeredPlugins.map(p => ({
      npmName: 'peertube-plugin-' + p.name,
      total: p.total
    })),

    registeredThemes: body.registeredThemes.map(t => ({
      npmName: 'peertube-theme-' + t.name,
      total: t.total
    }))
  } as { registeredPlugins: RegisteredPluginStat[], registeredThemes: RegisteredPluginStat[] }
}
