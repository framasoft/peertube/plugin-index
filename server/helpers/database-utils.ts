import retry from 'async/retry'
import { logger } from './logger.js'

function retryTransactionWrapper <T> (
  functionToRetry: (...args: any[]) => Promise<T>,
  ...args: any[]
): Promise<T> {
  return transactionRetryer<T>(callback => {
    functionToRetry.apply(null, args)
        .then((result: T) => callback(null, result))
        .catch(err => callback(err))
  })
  .catch(err => {
    logger.error({ err }, `Cannot execute ${functionToRetry.name} with many retries.`)
    throw err
  })
}

function transactionRetryer <T> (func: (err: any, data: T) => any) {
  return new Promise<T>((res, rej) => {
    retry({
      times: 5,

      errorFilter: err => {
        const willRetry = (err.name === 'SequelizeDatabaseError')
        logger.debug({ willRetry }, 'Maybe retrying the transaction function.')
        return willRetry
      }
    }, func, (err, data) => err ? rej(err) : res(data))
  })
}

// ---------------------------------------------------------------------------

export {
  retryTransactionWrapper,
  transactionRetryer
}
