import express from 'express'
import { ResultList } from '@peertube/peertube-types'

function badRequest (req: express.Request, res: express.Response) {
  return res.type('json').status(400).end()
}

interface FormattableToJSON {
  toFormattedJSON: () => any
}

function getFormattedObjects<U, T extends FormattableToJSON> (objects: T[], objectsTotal: number) {
  const formattedObjects: U[] = []

  objects.forEach(object => {
    formattedObjects.push(object.toFormattedJSON())
  })

  return {
    total: objectsTotal,
    data: formattedObjects
  } as ResultList<U>
}

// ---------------------------------------------------------------------------

export {
  badRequest,
  getFormattedObjects
}
