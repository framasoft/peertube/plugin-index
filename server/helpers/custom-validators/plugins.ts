import validator from 'validator'
import { PluginType_Type } from '@peertube/peertube-types'
import { CONSTRAINTS_FIELDS } from '../../initializers/constants.js'
import { logger } from '../logger.js'
import { isUrlValid } from './activitypub/misc.js'
import { exists, isArray, isSafePath } from './misc.js'
import { PluginType } from '../../types/index.js'

const PLUGINS_CONSTRAINTS_FIELDS = CONSTRAINTS_FIELDS.PLUGINS

function isPluginTypeValid (value: any) {
  return exists(value) && validator.isInt('' + value) && [ PluginType.PLUGIN, PluginType.THEME ].includes(value)
}

function isPluginNameValid (value: string) {
  return exists(value) &&
    validator.isLength(value, PLUGINS_CONSTRAINTS_FIELDS.NAME) &&
    validator.matches(value, /^[a-z-]+$/)
}

function isNpmPluginNameValid (value: string) {
  return exists(value) &&
    validator.isLength(value, PLUGINS_CONSTRAINTS_FIELDS.NAME) &&
    validator.matches(value, /^[a-z\-._0-9]+$/) &&
    (value.startsWith('peertube-plugin-') || value.startsWith('peertube-theme-'))
}

function isPluginDescriptionValid (value: string) {
  return exists(value) && validator.isLength(value, PLUGINS_CONSTRAINTS_FIELDS.DESCRIPTION)
}

function isPluginVersionValid (value: string) {
  if (!exists(value)) return false

  // We don't need -rc.1 -beta etc
  const firstPart = value.split('-')[0]
  const parts = (firstPart + '').split('.')

  return parts.length === 3 && parts.every(p => validator.isInt(p))
}

function arePluginNpmNamesValid (values: any) {
  return isArray(values) && values.length < 50 && values.every(v => isNpmPluginNameValid(v))
}

function isPluginEngineValid (engine: any) {
  return exists(engine) && exists(engine.peertube)
}

function isPluginHomepage (value: string) {
  return isUrlValid(value)
}

function isStaticDirectoriesValid (staticDirs: any) {
  if (!exists(staticDirs) || typeof staticDirs !== 'object') return false

  for (const key of Object.keys(staticDirs)) {
    if (!isSafePath(staticDirs[key])) return false
  }

  return true
}

function isClientScriptsValid (clientScripts: any[]) {
  return isArray(clientScripts) &&
    clientScripts.every(c => {
      return isSafePath(c.script) && isArray(c.scopes)
    })
}

function isCSSPathsValid (css: any[]) {
  return isArray(css) && css.every(c => isSafePath(c))
}

function isThemeNameValid (name: string) {
  return isPluginNameValid(name)
}

function areTranslationPathsValid (translations: any) {
  if (!exists(translations) || typeof translations !== 'object') return false

  for (const key of Object.keys(translations)) {
    if (!isSafePath(translations[key])) return false
  }

  return true
}

function sanitizeAndCheckPackageJSONValid (packageJSON: any, pluginType: PluginType_Type) {
  if (packageJSON?.bugs?.url) packageJSON.bugs = packageJSON.bugs.url

  if (!packageJSON.bugs) packageJSON.bugs = packageJSON.homepage
  if (!packageJSON.staticDirs) packageJSON.staticDirs = {}
  if (!packageJSON.css) packageJSON.css = []
  if (!packageJSON.clientScripts) packageJSON.clientScripts = []
  if (!packageJSON.translations) packageJSON.translations = {}

  if (!isNpmPluginNameValid(packageJSON.name)) {
    logger.debug('packageJSON name is invalid.')
    return false
  }

  if (!isPluginDescriptionValid(packageJSON.description)) {
    logger.debug('packageJSON description is invalid')
    return false
  }

  if (!isPluginEngineValid(packageJSON.engine)) {
    logger.debug('packageJSON engine is invalid')
    return false
  }

  if (!isPluginHomepage(packageJSON.homepage)) {
    logger.debug('packageJSON homepage is invalid')
    return false
  }

  if (!exists(packageJSON.author)) {
    logger.debug('packageJSON author is invalid')
    return false
  }

  if (!isUrlValid(packageJSON.bugs)) {
    logger.debug('packageJSON bugs is invalid')
    return false
  }

  if (!(pluginType === PluginType.THEME || isSafePath(packageJSON.library))) {
    logger.debug('packageJSON library is not a safe path')
    return false
  }

  if (!isStaticDirectoriesValid(packageJSON.staticDirs)) {
    logger.debug('packageJSON staticDirs is invalid')
    return false
  }

  if (!isCSSPathsValid(packageJSON.css)) {
    logger.debug('packageJSON css is invalid')
    return false
  }

  if (!isClientScriptsValid(packageJSON.clientScripts)) {
    logger.debug('packageJSON client scripts is invalid')
    return false
  }

  if (!areTranslationPathsValid(packageJSON.translations)) {
    logger.debug('packageJSON translations paths is invalid')
    return false
  }

  return true
}

function isLibraryCodeValid (library: any) {
  return typeof library.register === 'function' &&
    typeof library.unregister === 'function'
}

export {
  isPluginTypeValid,
  sanitizeAndCheckPackageJSONValid,
  isThemeNameValid,
  isPluginHomepage,
  arePluginNpmNamesValid,
  isPluginVersionValid,
  isPluginNameValid,
  isPluginDescriptionValid,
  isLibraryCodeValid,
  isNpmPluginNameValid
}
