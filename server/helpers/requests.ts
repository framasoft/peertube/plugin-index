import axios from 'axios'

export function doJSONRequest (url: string) {
  return axios({
    url,
    responseType: 'json'
  }).then(res => ({ body: res.data }))
}
